/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author CSIE
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    
    Connection conn =null;
    @FXML
    private void handleButtonAction(ActionEvent event) {
            doConn();
            //int row =doInsert();
            //label.setText("row" + "Add" + row);
            int row = doSelect();
            label.setText("row" + "Add" + row);
        }

      
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }  
     public void doConn() {
        try {
            conn = DriverManager.getConnection("jdbc:derby://localhost:1527/contact","test","test");
            if(conn != null){
                label.setText("Linked");
             
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                label.setText("Failed");
        }
                
    }  
    
    private int doInsert(){
    int rows =0;
    String insert="INSERT INTO TEST.COLLEAGUES (ID,FIRSTNAME,LASTNAME,TITLE,DEPARTMENT,EMAIL,LOGIN) VALUES (?,?,?,?,?,?,?)"; 
    PreparedStatement stmt = null;
    try {
            stmt = conn.prepareStatement(insert);
            stmt.setInt(1,7);
            stmt.setString(2, "test");
            stmt.setString(3, "test");
            stmt.setString(4, "test");
            stmt.setString(5, "test");
            stmt.setString(6, "test");
            stmt.setInt(7, 8);
            rows=stmt.executeUpdate();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);                
        } finally {
        try {
            stmt.close();
            conn.close();
            } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
             }
         return rows;   
        }  
    }

    private int doSelect(){
        int rows = 0;
        String  insert = "SELECT * FROM TEST.COLLEAGUES WHERE ID > ?";
       PreparedStatement stmt = null;
       ResultSet rs =null;
       try {
            stmt = conn.prepareStatement(insert);
            stmt.setInt(1,0);
            rs = stmt.executeQuery();
            while(rs.next()){
                System.out.println(rs.getString("FIRSTNAME"));
                rows++;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);                
        } finally {
        try {
            rs.close();
            stmt.close();
            conn.close();
            } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
             }
         return rows;   
        }  
    
    }
}
